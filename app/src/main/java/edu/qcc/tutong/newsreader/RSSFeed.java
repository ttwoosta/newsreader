package edu.qcc.tutong.newsreader;

/*
This class stores the data for the RSS feed
 */

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.annotation.SuppressLint;

@SuppressLint("SimpleDateFormat")
public class RSSFeed {
    private String title = null;
    private String pubDate = null;
    private ArrayList<RSSItem> items;
        
    private SimpleDateFormat dateInFormat = 
            new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z");

    /* initializer */
    public RSSFeed() {
        items = new ArrayList<RSSItem>(); 
    }

    // gets and sets the title for the feed
    public void setTitle(String title) {
        this.title = title;
    }
    public String getTitle() {
        return title;
    }

    // gets and set Publication date for the feed
    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }
    public String getPubDate() {
        return pubDate;
    }

    // get the publication in milliseconds
    public long getPubDateMillis() {
        try {
            Date date = dateInFormat.parse(pubDate.trim());
            return date.getTime();
        }
        catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    // add new rss item
    public int addItem(RSSItem item) {
        items.add(item);
        return items.size();
    }

    // get the item at a specified index
    public RSSItem getItem(int index) {
        return items.get(index);
    }

    public ArrayList<RSSItem> getAllItems() {
        return items;
    }    
}