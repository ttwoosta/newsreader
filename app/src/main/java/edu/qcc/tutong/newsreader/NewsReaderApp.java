package edu.qcc.tutong.newsreader;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * Created by tutong on 4/5/18.
 */

public class NewsReaderApp extends Application {

    // Define the instance variables for the preferences
    private SharedPreferences sharedPrefs;

    // the tag uses for logging
    private static final String TAG = NewsReaderApp.class.getSimpleName();

    // Feed milliseconds that correspond with the publication date
    private long feedMillis = -1;
    public long getFeedMillis() { return feedMillis; }
    public void setFeedMillis(long feedMillis) {

        // save the value to disk
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putLong("feedMillis", feedMillis);
        editor.apply();

        // update the instance variable
        this.feedMillis = feedMillis;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("New reader", "App started");

        // Get the SharedPreferences object
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        // retrieve the time since last download feed
        feedMillis = sharedPrefs.getLong("feedMillis", 0);
    }
}
