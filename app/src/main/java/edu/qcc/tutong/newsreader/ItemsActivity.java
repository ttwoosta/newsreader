package edu.qcc.tutong.newsreader;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class ItemsActivity extends Activity implements AdapterView.OnItemClickListener {

    private static String URL_STRING = "https://www.ecommercetimes.com/perl/syndication/rssfull.pl";
    private static final  String TAG = ItemsActivity.class.getSimpleName();

    private TextView titleTextView;
    private ListView itemsListView;

    // xml file name
    final String FILENAME = "news_feed.xml";

    // the parsed feed
    RSSFeed feed;

    DownloadFeed downloadFeed;
    ReadFeed readFeed;

    // reference of Application object
    private NewsReaderApp app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items);

        // reference controls
        titleTextView = (TextView)findViewById(R.id.titleTextView);
        itemsListView = (ListView)findViewById(R.id.itemsListView);

        // ListView item click handler
        itemsListView.setOnItemClickListener(this);

        // get a reference of the application object
        app = (NewsReaderApp)getApplication();
        // calculate how long ago the feed is downloaded
        long diff = (new Date()).getTime() - app.getFeedMillis();
        if (diff > 1000*24*60*60) {
            // download feed
            downloadFeed = new DownloadFeed();
            downloadFeed.execute(URL_STRING);
        }
        else {
            readFeed = new ReadFeed();
            readFeed.execute();
        }
    }


    class DownloadFeed extends AsyncTask<String, Integer, String> {

        private final String TAG = ItemsActivity.class.getSimpleName();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d(TAG, "Task is executed");
        }

        @Override
        protected String doInBackground(String... params) {

            // get the parameter
            String urlString = params[0];

            // download the feed and write it to a file
            try {
                // get the input stream
                URL url = new URL(urlString);
                InputStream in = url.openStream();

                // get the output stream
                // file is stored /data/data/edu.qcc.tutong.newreader.files/
                FileOutputStream out =  openFileOutput(FILENAME, Context.MODE_PRIVATE);

                // read input and write output
                byte[] buffer = new byte[1024];
                int bytesRead = in.read(buffer);
                while ( bytesRead != -1) {
                    out.write(buffer, 0, bytesRead);
                    bytesRead = in.read(buffer);
                }
                out.close();
                in.close();

                // update the time downloaded feed
                Date currentDate = new Date();
                app.setFeedMillis(currentDate.getTime());
            }
            catch (IOException e) {
                Log.e(TAG, e.toString());
                return e.toString();
            }

            // return a message
            Log.d(TAG, "Feed downloaded");
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            Log.d(TAG, "Downloading feed: " + values.toString());
        }

        @Override
        protected void onPostExecute(String result) {
            //Context context = ItemsActivity.this;
            //Toast.makeText(context, result, Toast.LENGTH_LONG).show();
            if (result == null) {
                ItemsActivity.this.readFeed = new ReadFeed();
                ItemsActivity.this.readFeed.execute();
            }
            else {
                Log.e(TAG, "Error: " + result);
            }
        }
    }

    class ReadFeed extends AsyncTask<String, Integer, RSSFeed> {

        private final String TAG = ItemsActivity.class.getSimpleName();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected RSSFeed doInBackground(String... strings) {

            try {
                // get the XML reader
                SAXParserFactory factory = SAXParserFactory.newInstance();
                SAXParser parser = factory.newSAXParser();
                XMLReader xmlReader = parser.getXMLReader();

                // set the content handler
                RSSFeedHandler handler = new RSSFeedHandler();
                xmlReader.setContentHandler(handler);

                // get the input stream
                FileInputStream in = openFileInput(FILENAME);

                // parse the data
                InputSource is = new InputSource(in);
                xmlReader.parse(is);

                // get the content handler and return it
                Log.d(TAG, "Feed is read successfully");
                RSSFeed feed = handler.getFeed();
                return feed;
            }
            catch (Exception e) {
                Log.e(TAG, e.toString());
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(RSSFeed feed) {


            // update the display for the activity
            ItemsActivity.this.updateDisplayWithResult(feed);
        }


    }

    public void updateDisplayWithResult(RSSFeed feed) {

        // if result is null, operations are finished sucessfully
        // otherwise an error message if it failed
        if (feed == null) {
            titleTextView.setText("failed to read feed");
            return;
        }

        this.feed = feed;

        // set the title of the feed
        titleTextView.setText(feed.getTitle());

        // get the items for the feed
        ArrayList<RSSItem> items = feed.getAllItems();

        // create a List of Map<String, ?> objects
        ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();
        for (RSSItem item: items) {

            HashMap<String, String> map = new HashMap<String, String>();
            map.put("date", item.getPubDateFormatted());
            map.put("title", item.getTitle());
            map.put("subtitle", item.getDescription());
            data.add(map);
        }

        // create the resource, from, and to variables
        int resource = R.layout.listview_item;
        String[] from = {"date", "title", "subtitle"};
        int[] to = {R.id.pubDateTextView, R.id.titleTextView, R.id.subtitleTextView};

        // create and set the adapter
        SimpleAdapter adapter = new SimpleAdapter(this, data, resource, from, to);
        itemsListView.setAdapter(adapter);

        Log.d(TAG, "Feed displayed");
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        // get the item at the specified position
        RSSItem item = feed.getItem(position);

        // create an intent
        Intent intent = new Intent(this, ItemActivity.class);

        intent.putExtra("pubdate", item.getPubDate());
        intent.putExtra("title", item.getTitle());
        intent.putExtra("description", item.getDescription());
        intent.putExtra("link", item.getLink());

        this.startActivity(intent);
    }
}
