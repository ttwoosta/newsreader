package edu.qcc.tutong.newsreader;

import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class TimerActivity extends Activity implements View.OnClickListener {

    private Button startTaskButton;
    private TextView messageTextView;
    private static final String TAG = TimerActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);

        startTaskButton = (Button)findViewById(R.id.timerTaskButton);
        messageTextView = (TextView)findViewById(R.id.messageTextView);

        // set event handler
        startTaskButton.setOnClickListener(this);
    }

    private void updateView(final long elapseMillis) {
        // UI changes need to be run on the UI thread
        /* the 'post' method accepts any object
         that implements the Runnable interface */
        messageTextView.post(new Runnable() {

            // here are still in Timer thread
            int elapsedSeconds = (int)elapseMillis/1000;

            @Override
            public void run() { // main thread block
                messageTextView.setText("Seconds: " + elapsedSeconds);
            }
        });
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.timerTaskButton) {
            Log.d(TAG, "Start task button clicked");

            // the time when button is clicked
            final long startMillis = System.currentTimeMillis();

            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    long elapseMillis = System.currentTimeMillis() - startMillis;
                    updateView(elapseMillis);
                }
            };

            // create a Timer object that ends when the app ends
            Timer timer = new Timer(true);
            timer.schedule(task, 0, 1000); // execute every second without delay
        }

    }
}
