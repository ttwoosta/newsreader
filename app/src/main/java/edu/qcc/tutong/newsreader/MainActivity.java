package edu.qcc.tutong.newsreader;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.nio.charset.Charset;
import java.util.Date;

public class MainActivity extends Activity implements View.OnClickListener {

    // declare widget variables
    private Button asyncTaskButton;
    private Button timedTaskButton;
    private TextView feedInfoTextView;
    private Button startServiceButton;
    private Button stopServiceButton;
    private Button createNotificationButton;
    private TextView networkStatusTextView;
    private Button checkNetworkConnection;

    // the intent object for the service
    Intent serviceIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        asyncTaskButton = (Button)findViewById(R.id.asyncTaskButton);
        timedTaskButton = (Button)findViewById(R.id.timedTaskButton);
        feedInfoTextView = (TextView)findViewById(R.id.feedInfoTextView);

        // reference the 2 buttons that control service
        startServiceButton = (Button)findViewById(R.id.startServiceButton);
        stopServiceButton = (Button)findViewById(R.id.stopServiceButton);

        createNotificationButton = (Button)findViewById(R.id.createNotificationButton);
        checkNetworkConnection = (Button)findViewById(R.id.checkNetworkConnection);
        networkStatusTextView = (TextView)findViewById(R.id.networkStatusTextView);

        // setup event listener
        asyncTaskButton.setOnClickListener(this);
        timedTaskButton.setOnClickListener(this);
        startServiceButton.setOnClickListener(this);
        stopServiceButton.setOnClickListener(this);
        createNotificationButton.setOnClickListener(this);
        checkNetworkConnection.setOnClickListener(this);

        // get a reference to the Application object
        NewsReaderApp app = (NewsReaderApp)getApplication();
        // get the time last download the feed
        long feedMillis = app.getFeedMillis();
        if (feedMillis != 0) {
            Date feedDate = new Date(feedMillis);
            // set to TextView
            feedInfoTextView.setText(feedDate.toString());
        }
        else {
            feedInfoTextView.setText("The feed has not been download.");
        }

        // create the Intent object for the service
        serviceIntent = new Intent(this, NewsReaderService.class);

    }

    @Override
    public void onClick(View view) {

        // new intent that is used to start new activity
        Intent intent;

        switch (view.getId()) {

            case R.id.asyncTaskButton:
                intent = new Intent(MainActivity.this, ItemsActivity.class);
                startActivity(intent);
                break;
            case R.id.timedTaskButton:
                intent = new Intent(MainActivity.this, TimerActivity.class);
                startActivity(intent);
                break;
            case R.id.startServiceButton:
                startService(serviceIntent);
                break;
            case R.id.stopServiceButton:
                stopService(serviceIntent);
                break;
            case R.id.createNotificationButton:
                // get the Notificaiton manger object
                Notification notification = createNotification(createPendingIntent());
                NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

                // display a notification
                final int NOTIFICATION_ID = 1;
                manager.notify(NOTIFICATION_ID, notification);

                break;
            case R.id.checkNetworkConnection:
                // get NetworkInfo object
                ConnectivityManager connManager = (ConnectivityManager)
                        getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connManager.getActiveNetworkInfo();

                // if network is connected, download feed
                if (networkInfo != null && networkInfo.isConnected()) {
                    networkStatusTextView.setText("You're on " +
                            networkInfo.getTypeName() + " network, it's " +
                            networkInfo.getReason());
                }
                break;
        }
    }

    /* Create a pending intent that can be passed to other apps so that they can
       execute the intent at a later time */
    public PendingIntent createPendingIntent() {

        // create the intent
        Intent notificationIntent = new Intent(this, ItemActivity.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // create the pending intent
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        return pendingIntent;
    }

    /*
    * create and return notification object.
    * */
    public Notification createNotification(PendingIntent pendingIntent) {
        // create the variables for the notification
        int icon = R.drawable.ic_launcher;
        CharSequence tickerText = "Updated news feed is available";
        CharSequence contentTitle = getText(R.string.app_name);
        CharSequence contentText = "Select to view supdated feed.";

        // create the notification object
        Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(icon);
        builder.setTicker(tickerText);
        builder.setContentTitle(contentTitle);
        builder.setContentText(contentText);
        builder.setContentIntent(pendingIntent);
        builder.setAutoCancel(true);

        // build the notification object
        Notification notification = builder.build();

        // return the notification object
        return notification;
    }

}
