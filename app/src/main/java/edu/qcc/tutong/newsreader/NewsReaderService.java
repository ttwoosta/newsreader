package edu.qcc.tutong.newsreader;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.TextView;

public class NewsReaderService extends Service {

    // the tag uses for logging
    private static final String TAG = NewsReaderService.class.getSimpleName();


    public NewsReaderService() {
        Log.i(TAG, "Service is initialized");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // Code that's executed once when the service is created
        Log.i(TAG, "Service is created");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Code that's executed each time another component
        // starts the service by calling the startService method.
        Log.i(TAG, "Service is running");
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Code that's excuted each time another component
        // binds to the service by calling the bindService method.
        // This method is required.
        // For an unbound service, you can return a null value.
        return null;
    }

    @Override
    public void onDestroy() {
        // Code that's executed once when that service
        // is no longer in use and is being destroyed.
        Log.i(TAG, "Service is destroyed");
    }
}
