package edu.qcc.tutong.newsreader;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.TextView;

public class ItemActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);

        // get references to widgets
        TextView titleTextView = (TextView)findViewById(R.id.titleTextView);
        TextView pubDateTextView = (TextView)findViewById(R.id.pubDateTextView);
        TextView descriptionTextView = (TextView)findViewById(R.id.descriptionTextView);
        TextView linkTextView = (TextView)findViewById(R.id.linkTextView);

        // get the intent and its data
        Intent intent = getIntent();
        String pubDate = intent.getStringExtra("pubdate");
        String title = intent.getStringExtra("title");
        String description = intent.getStringExtra("description").replace('\n', ' ');

        // display data on the widgets
        pubDateTextView.setText(pubDate);
        titleTextView.setText(title);
        descriptionTextView.setText(description);

        // set the listener
        linkTextView.setOnClickListener((View.OnClickListener) this);
    }


    @Override
    public void onClick(View view) {
        // get the intent and create the Uri for the link
        Intent intent = getIntent();
        String link = intent.getStringExtra("link");
        Uri viewUri = Uri.parse(link);

        // create the intent and start it
        Intent viewIntent = new Intent(Intent.ACTION_VIEW, viewUri);
        startActivity(viewIntent);
    }
}
